const fs = require('fs');
const config = require('./config.js');
const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const secretKey = crypto.createHash('sha256').update(String(config.CRYPTO_SECRET)).digest('base64').substr(0, 32);
const iv = crypto.createHash('sha256').update(String(config.CRYPTO_IV)).digest('base64').substr(0, 16);
const MaskData = require('maskdata');

/**
 * Delete image in images folder.
 * @param {string} imageUrl Url at image to delete.
 * @returns {void | object} Return a Error object if fail.
 */
exports.deleteImage = (imageUrl) => {
    try {
        const filename = imageUrl.split('/images/')[1];
        fs.unlinkSync(`images/${filename}`);
    } catch (error) {
        throw error;
    }
};

/**
 * Crypt a text.
 * @param {string} text Text to crypt. 
 * @returns Text crypted.
 */
exports.encrypt = (text) => {
    const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
    return encrypted.toString('hex');
}

/**
 * Decrypt a text.
 * @param {string} text Text to decrypt. 
 * @returns Text decrypted.
 */
exports.decrypt = (text) => {
    const decipher = crypto.createDecipheriv(algorithm, secretKey, iv);
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(text, 'hex')), decipher.final()]);
    return decrpyted.toString();
}

exports.maskEmail = (email) => {
    const emailMask2Options = {
        maskWith: "*", 
        unmaskedStartCharactersBeforeAt: 10,
        unmaskedEndCharactersAfterAt: 0,
        maskAtTheRate: false
    };
    return MaskData.maskEmail2(email, emailMask2Options);
}