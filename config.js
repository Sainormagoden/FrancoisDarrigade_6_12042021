module.exports = {
    DB_HOST : process.env.DB_HOST,
    DB_USER : process.env.DB_USER,
    DB_PASS : process.env.DB_PASS,
    DB_NAME : process.env.DB_NAME,

    JWT_TOKEN : process.env.JWT_TOKEN,
    SESSION_SECRET : process.env.SESSION_SECRET,
    SESSION_NAME : process.env.SESSION_NAME,
    CRYPTO_SECRET : process.env.CRYPTO_SECRET,
    CRYPTO_IV : process.env.CRYPTO_IV
}