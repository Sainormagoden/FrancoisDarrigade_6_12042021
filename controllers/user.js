const bcrypt = require('bcrypt');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const sanitize = require('mongo-sanitize');
const config = require('../config.js');
const utils = require('../utils');

/**
 * Encrypt user password and add user in database.
 */ 
exports.signUser = (req, res) => {
    
    // Test if good password and mail.
    const regexMail = new RegExp('^(([^<>()[\\]\\\\.,;:\\s@"]+(\\.[^<>()[\\]\\\\.,;:\\s@"]+)*)|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$');
    const regexPassword = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$');
    if (!regexPassword.test(req.body.password)) {
        return res.status(401).json({ error: 'Password must contain at least 8 characters, including 1 uppercase, 1 lowercase and 1 number.' });
    }
    if (!regexMail.test(req.body.email)) {
        return res.status(401).json({ error: 'Bad email send !' });
    }

    // Crypt password.
    bcrypt.hash(req.body.password, 10)
        .then(hash => {
            const user = new User({
                email: utils.encrypt(sanitize(req.body.email)),
                email_mask: utils.maskEmail(sanitize(req.body.email)),
                password: hash
            });
            user.save()
                .then(() => res.status(201).json({ message: 'Save user' }))
                .catch(error => res.status(400).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};

/**
 * Check the information identification of the user.
 * Send user ID and Token
 */ 
exports.logUser = (req, res) => {
    User.findOne({email : utils.encrypt(sanitize(req.body.email))})
        .then(user => {
            if (!user) {
                return res.status(401).json({ error: 'User not found !'});
            }
            bcrypt.compare(sanitize(req.body.password), user.password)
                .then(valid => {
                    if (!valid) {
                        return res.status(401).json({ error: 'Incorrect password !'});
                    }
                    const token = jwt.sign({ userId: user._id }, config.JWT_TOKEN, { expiresIn: '24h' });
                    res.status(200).json({
                        userId: user._id,
                        token
                    });
                })
                .catch(error => res.status(500).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};