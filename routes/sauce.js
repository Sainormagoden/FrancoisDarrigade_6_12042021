const express = require('express');
const router = express.Router();
const sauceCtrl = require('../controllers/sauce');
const auth = require('../middleware/auth');
const multer = require('../middleware/multer-config.js');

// Get all sauces.
router.get('/' + '', auth, sauceCtrl.getAllSauces);

// Get one sauce.
router.get('/:id', auth, sauceCtrl.getOneSauce);

// Post a new sauce with a image.
router.post('/', auth, multer, sauceCtrl.postSauce);

// Modify a sauce with potential image.
router.put('/:id', auth, multer, sauceCtrl.putSauce);

// Delete a sauce.
router.delete('/:id', auth, sauceCtrl.deleteSauce);

// Add or remove a like/dislike 
router.post('/:id/like', auth, sauceCtrl.postSauceLike);

module.exports = router;