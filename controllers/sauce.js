const Sauce = require('../models/sauce');
const utils = require('../utils');

/**
 * Get all sauces.
 */
exports.getAllSauces = (req, res) => {  
    Sauce.find()
        .then(sauces => res.status(200).json(sauces))
        .catch(error => res.status(400).json({ error }));
};

/**
 * Get one sauces
 */ 
exports.getOneSauce = (req, res) => {
    Sauce.findOne({ _id: req.params.id })
        .then(sauce => res.status(200).json(sauce))
        .catch(error => res.status(404).json({ error }));
};

/**
 * Save a new sauce.
 * Clear all likes, dislikes, user likes and user dislikes.
 */
exports.postSauce = (req, res) => {
    const sauceObject = JSON.parse(req.body.sauce);
    const sauce = new Sauce({ 
        ...sauceObject,
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    });
    sauce.save()
        .then(() => res.status(201).json({ message: 'Save your sauce' }))
        .catch(error => res.status(500).json({ error }));
};

/**
 * Modify a sauce.
 */ 
exports.putSauce = (req, res) => {
    if (req.file) {
        const sauceObject = {
            ...JSON.parse(req.body.sauce),
            imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
        };
        Sauce.findOne({ _id: req.params.id })
            .then(sauce => {
                utils.deleteImage(sauce.imageUrl);
                Sauce.updateOne({ _id: req.params.id }, { ...sauceObject})
                .then(() => res.status(200).json({ message: 'Save modify sauce' }))
                .catch(error => res.status(400).json({ error }));
            })
            .catch(error => res.status(400).json({ error }));
    } else {
        const sauceObject = {
            ...req.body
        };
        Sauce.updateOne({ _id: req.params.id }, { ...sauceObject})
            .then(() => res.status(200).json({ message: 'Save modify sauce' }))
            .catch(error => res.status(400).json({ error }));
    }
};

/**
 * DELETE a sauce with its image.
 */ 
exports.deleteSauce = (req, res) => {
    Sauce.findOne({ _id: req.params.id })
        .then(sauce => {
            utils.deleteImage(sauce.imageUrl);
            Sauce.deleteOne({ _id: req.params.id })
                .then(() => res.status(200).json({ message: 'Sauce deleted !'}))
                .catch(error => res.status(400).json({ error }));
        })
        .catch(error => res.status(400).json({ error }));
};

/**
 * POST a like for a sauce.
 */ 
exports.postSauceLike = (req, res) => {
    const sauceId = req.params.id;
    let like;
    let likeArray;

    Sauce.findOne({ _id: sauceId })
        .then(sauce => {
            switch (req.body.like) {
                case 1 : {
                    if (!sauce.usersLiked.indexOf(req.body.userId) !== -1) {
                        likeArray = sauce.usersLiked;
                        likeArray.push(req.body.userId);
                        like = {
                            'likes' : sauce.likes+1,
                            'usersLiked' : likeArray
                        };
                    } else {
                        return res.status(400).json({ error: 'You have already liked'});
                    }
                    break;
                }
                case 0 : {
                    if (sauce.usersLiked.indexOf(req.body.userId) !== -1) {
                        likeArray = sauce.usersLiked;
                        likeArray.splice(likeArray.indexOf(req.body.userId), 1);
                        like = {
                            'likes' : sauce.likes-1,
                            'usersLiked' : likeArray,
                        };
                    } else if (sauce.usersDisliked.indexOf(req.body.userId) !== -1) {
                        likeArray = sauce.usersDisliked;
                        likeArray.splice(likeArray.indexOf(req.body.userId), 1);
                        like = {
                            'dislikes' : sauce.dislikes-1,
                            'usersDisliked' : likeArray
                        };
                    } else {
                        return res.status(404).json({ error: 'Precedent like or dislike not found'});
                    }
                    break;
                }
                case -1 : {
                    if (!sauce.usersDisliked.indexOf(req.body.userId) !== -1) {
                        likeArray = sauce.usersDisliked;
                        likeArray.push(req.body.userId);
                        like = {
                            'dislikes' : sauce.dislikes+1,
                            'usersDisliked' : likeArray
                        };
                    } else {
                        return res.status(400).json({ error: 'You have already disliked'});
                    }
                    break;
                }
                default : {
                    return res.status(400).json({ error: 'Bad number for like send'});
                }
            }
            Sauce.updateOne({ _id: sauceId }, {...like})
                .then(() => res.status(200).json({ message: 'Saved like/dislike' }))
                .catch(error => res.status(400).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};