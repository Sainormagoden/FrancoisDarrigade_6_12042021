const sanitize = require('mongo-sanitize');

/**
 * Check type of values in req.body.
 */
module.exports = (req, res, next) => {
    try {
        let email = sanitize(req.body.email);
        let password = sanitize(req.body.password);
        if (typeof email === 'string' && typeof password === 'string') {
            next();
        } else {
            res.status(401).json({ error: 'Invalid request!' });
        }
    } catch {
        res.status(401).json({ error: 'Invalid request!' });
    }
};