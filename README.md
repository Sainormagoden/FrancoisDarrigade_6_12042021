This is a API for back-end of project 6 of OpenClassrooms for developper web.

This API use : Express, NodeJS server and MongoDB.

For download front-end : https://github.com/OpenClassrooms-Student-Center/dwj-projet6

For use this api, you need create a .env (help in 'example.env'),
and use node command : 
    -npm install
    -nodemon server

The link for access to API, by default, is : http://localhost:3000