const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/user');
const injection = require('../middleware/injection');

// Sign a new user.
router.post('/signup', injection, userCtrl.signUser);

// Log user.
router.post('/login', injection, userCtrl.logUser);

module.exports = router;